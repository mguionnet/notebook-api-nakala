# Notebook API NAKALA

Ce notebook Jupyter a été mis en place à l'occasion des [Rencontres Huma-NUM 2021](https://humanum.hypotheses.org/6565). Il a été réalisé à l'aide de [Callisto](https://callisto.huma-num.fr), preuve de concept du [HN Lab](https://www.huma-num.fr/hnlab).

Il a pour but de présenter le fonctionnement de l'[API de NAKALA](https://api.nakala.fr/doc), le service d’Huma-Num permettant à des chercheurs, enseignants-chercheurs ou équipes de recherche de partager, publier et valoriser tous types de données numériques documentées (fichiers textes, sons, images, vidéos, objets 3D, etc.) dans un entrepôt sécurisé afin de les publier en accord avec les principes du FAIR data (Facile à trouver, Accessible, Interopérable et Réutilisable).

Ce notebook Jupyter peut être consulté en mode lecture seule depuis [nbviewer](https://nbviewer.jupyter.org/urls/gitlab.huma-num.fr/huma-num-public/notebook-api-nakala/-/raw/master/presentation-api.ipynb) ou en mode exécutable avec [![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fhuma-num-public%2Fnotebook-api-nakala/master?filepath=presentation-api.ipynb).

N'hésitez pas à nous écrire à nakala@huma-num.fr pour toute question ou remarque sur cette présentation.

Bonne lecture !

_Le pôle Accès d'HUMA-NUM_

[![Huma-Num](illustrations/logo-petit-hn-rvb.jpeg)](https://www.huma-num.fr/)
[![NAKALA](illustrations/logo-grand-nakala-rvb_0.jpeg)](https://nakala.fr/)

